
var gulp = require('gulp');
var shell = require('gulp-shell');

gulp.task('default', ['test']);

gulp.task('cordova', shell.task([
  'cd client/ && webpack --config=webpack.cordova.js',
]));

gulp.task('website', shell.task([
  'cd client/ && webpack --config=webpack.production.js',
]));

gulp.task('test', shell.task([
  'python manage.py test'
]));

var watcher = gulp.watch('rest/**/*.py', ['test']);

watcher.on('change', function(event) {
  console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
});
