from invoke import run, task
import paramiko


server = "leithall.com"


class Quiet:
    def __init__(self):
        self.server = server
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(
            paramiko.AutoAddPolicy())
        ssh.connect(server)
        self.ssh = ssh
        self.location = ""

    def run(self, cmd):
        if self.location:
            cmd = "cd {} && {}".format(self.location, cmd)
        stdin, stdout, stderr = self.ssh.exec_command(cmd)
        self.print(cmd)
        self.print("-" * 20)
        self.print_file(stdout)
        self.print_file(stderr)

    def cd(self, path):
        self.location = path

    def print(self, line):
        print("{}> {}".format(self.server, line))

    def print_file(self, fileh):
        for line in str(fileh.read(), "ascii").splitlines():
            self.print(line)
        print("")


@task
def deploy():
    ssh = Quiet()
    ssh.cd("/var/www/chores.leithall.com")
    ssh.run("git pull")
    ssh.run("touch config3.ini")


@task
def app():
    ssh = Quiet()
    ssh.cd("/var/www/chores.leithall.com")
    ssh.run("gulp website")
    ssh.run("venv-chores/bin/python manage.py collectstatic --noinput")
