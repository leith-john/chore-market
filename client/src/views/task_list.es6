var Backbone = require("backbone");
var bv = require("backbone_views");
var Provider = require("../models/provider");
var _ = require("underscore");
var nunjucks = require("nunjucks");


class StatusItem extends bv.DetailView {
  get template() {
    return _.template(`<span data-name></span>`);
  }
}

class TaskItem extends bv.DetailView {
  get tagName() { return "li"; }
  get mixins() {
    return [bv.Composite, require("../mixins/nmixin")];
  }
  get views() {
    return {
      ".status": StatusItem
    };
  }
  get detailSelector() {
    return ".detail";
  }
  get template() {
    return this.ntemplate(`
      <div class="label label-warning pull-right">
        {{ provider.name }}
      </div>
      <span class="text-muted">
        {% if status.default %}
          <i class="fa fa-square-o"></i>
        {% elif status.complete %}
          <i class="fa fa-check-square-o"></i>
        {% else %}
          <i class="fa fa-square"></i>
        {% endif %}
      </span>
      <span class="detail">
        <a href="{{ link }}">{{ name }}</a>
      </span>
      {% if next %}
        <div class="text-muted text-right">Due: {{ next|date }}</div>
      {% endif %}
    `);
  }

  initialize(options) {
    super.initialize(options);
    Backbone.app.setTitle();
  }

  createView(cls, options) {
    let status = this.model.attributes.status;
    if (status) {
      options.model = new bv.BaseModel(status);
      if (options.model) {
        return new cls(options);
      }
    }
    return null;
  }
}


class TaskList extends bv.ListView {
  get itemViewClass() {
    return TaskItem;
  }
  get collection() {
    return Backbone.app.collections.tasks;
  }
  get template() {
    return _.template(`
      <h2>Tasks</h2>
      <div class="empty alert alert-warning">
        Sorry, there are no tasks. <br>
        Maybe you can find some other task providers:
          <a href="#providers/">View task providers</a>
      </div>
      <ul class="task list"></ul>
    `);
  }

  initialize(options) {
    super.initialize(options);
    this.collection.fetch();
  }
}


module.exports = TaskList;
