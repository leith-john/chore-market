var Backbone = require("backbone");
var bv = require("backbone_views");
var UserReward = require("../models/user_reward");
var _ = require("underscore");
var nunjucks = require("nunjucks");
var Rx = require("rx");


class RewardItem extends bv.DetailView {
  get mixins() {
    return [bv.Composite, require("../mixins/nmixin")];
  }
  get template() {
    return this.ntemplate(`
      <span class="pull-right">
        <a href="#task/{{ task }}/">Go to task</a>
      </span>
      {{ amount }}: {{ currency.name }}
    `);
  }

  initialize(options) {
    super.initialize(options);
    this.listenTo(this.model, "change", () => {
      this.render();
    });
  }

  createView(cls, options) {
    let status = this.model.attributes.status;
    if (status) {
      options.model = new bv.BaseModel(status);
      if (options.model) {
        return new cls(options);
      }
    }
    return null;
  }
}


class RewardList extends bv.ListView {
  get mixins() { return [require("../mixins/nmixin")]; }
  get title() { return "My Rewards"; }
  get back() { return "Dashboard"; }
  get itemViewClass() {
    return RewardItem;
  }
  get template() {
    return _.template(`
      <h2><%- title %></h2>
      <div class="empty alert alert-warning">
        What? You haven't earned any rewards yet?<br>
        There is still time! Go get some tasks done!
        <a href="#tasks/">My Tasks</a>
      </div>
      <div class="totals"></div>
      <h3>Items</h3>
      <div class="list"></div>
    `);
  }
  get events() {
  }

  initialize(options) {
    this.collection = UserReward.collection();
    super.initialize(options);

    // TODO: make this functional reactive
    this.totals = Rx.Observable
      .fromEvent(this.collection, "sync")
      .map(function(c) {
        let retval = {};
        let all = {};
        for (let model of c.models) {
          let currency = model.attributes.currency;
          all[currency.id] = currency.name;
          if (! retval[currency.id]) {
            retval[currency.id] = 0;
          }
          retval[currency.id] += parseFloat(model.attributes.amount);
        }
        let entries = [];
        for (let k of Object.keys(retval)) {
          let v = retval[k];
          entries.push([v, all[k]]);
        }
        return entries;
      })
      .subscribe((c) => {

        console.log("subscribe", c);
        let template = this.ntemplate(`
          <h3>Totals</h3>
          <ul class="list-group">
            {% for key, value in totals %}
              <li class="list-group-item">
                {{ value }}
                <span class="badge badge-info">
                  {{ key }}
                </span>
              </li>
            {% endfor %}
          </ul>
        `);
        this.$(".totals").html(template({totals: c}));
      });

    this.collection.fetch();

    this.listenTo(this.collection, "sync", function() {console.log("foo")});
  }

  remove() {
    this.totals.dispose();
  }
}

module.exports = RewardList
