var Backbone = require("backbone");
var bv = require("backbone_views");
var Provider = require("../models/provider");
var _ = require("underscore");


class Login extends bv.DetailView {
  get template() {
    return _.template(`
    <div class="jumbotron">
      <div class="container">
        <h2>Login</h2>
        <form class="">
          <div class="alert alert-danger hide">
            Error!
          </div>
          <div class="form-group">
            <label>Username:</label>
            <input type="text" name="username" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Password:</label>
            <input type="password" name="password" class="form-control" required>
          </div>
          <button class="login-btn btn btn-primary">
            Login
          </button>
        </form>
      </div>
    </div>
    `);
  }
  get events() {
    return {
      "submit form": "handleLogin",
    };
  }

  initialize(options) {
    this.model = Backbone.app.user;
    super.initialize(options);
  }
  
  handleLogin(e) {

    // hide any previous errors
    this.$(".alert-danger").addClass("hide");

    e.preventDefault();
    let username = this.$("[name=username]").val();
    let password = this.$("[name=password]").val();
    this.model.login(username, password).then(() => {
      let next = Backbone.app.session.get("next", "/");
      if (next == "#login/") {
        next = "/";
      }
      Backbone.app.go(next);
      console.log("logged in");
    }).catch((e) => {
      let error = "There was an error communciating with the server. " +
        "Please try again later";

      if (e && e.non_field_errors) {
        error = e.non_field_errors;
      }

      this.$(".alert-danger").text(error).removeClass("hide");
    });
  }
}


module.exports = Login
