var Backbone = require("backbone");
var bv = require("backbone_views");
var Task = require("../models/task");
var _ = require("underscore");
var toastr = require("toastr");


class StatusDetail extends bv.DetailView {
  get template() {
    return _.template(`
      <button data-can_change="prop-inverse:disabled"
              class="btn btn-default">
        <span data-can_change="inverse-toggle">
          <i class="fa fa-check-square-o"></i>
        </span>
        <span data-can_change="toggle">
          <%= button %>
        </span>
        <span data-can_change="inverse-toggle">
          <span data-name></span>
        </span>
      </button>
    `);
  }

  getContext(options) {
    let next = this.model.attributes.next;
    options.button = next && next.name || this.model.attributes.name;
    return options;
  }

  createView(cls, options) {
    if (this.model.attributes.next) {
      options.model = new bv.BaseModel(this.model.attributes.next);
      return new cls(options);
    } else {
      return null;
    }
  }
}


class RewardItem extends bv.DetailView {
  get template() {
    return _.template(`
      <span data-currency class="col-xs-6"></span>
      <span data-amount class="col-xs-6"></span>
    `);
  }
  initialize(options) {
    super.initialize(options);
    this.setElement("<div class='row'></div>");
  }
}


class RewardList extends bv.ListView {
  get template() {
    return _.template(`
      <h3 class="exists">You will be rewarded</h3>
      <div class="list"></div>
    `);
  }
  get itemViewClass() {
    return RewardItem;
  }
}


class TaskDetail extends bv.DetailView {
  get back() { return {title: "Tasks", url: "tasks/"}; }
  get mixins() {
    return [bv.Composite];
  }
  get views() {
    return {
      ".status": StatusDetail,
      ".rewards": RewardList,
    };
  }
  get detailSelector() {
    return ".task-detail";
  }
  get template() {
    return _.template(`
      <div class="task-detail">
        <div class="pull-right text-muted">#<span data-id></span></div>
        <h2 data-name></h2>
        <div data-starting="toggle" class="text-small">
          Starting: <span data-starting></span>
        </div>
        <div data-next="toggle" class="text-small">
          Next: <span data-next></span>
        </div>
        <div data-description></div>
      </div>
      <hr>
      <div class="rewards"></div>
      <div class="status"></div>
    `);
  }
  get events() {
    return {
      "click button": "handleComplete",
    };
  }
  get collection() {
    return Backbone.app.collections.tasks;
  }

  initialize(options) {
    this.model = this.collection.get(options.kwargs.id)
    if (! this.model) {
      this.model = new Task({id: options.kwargs.id})
      this.collection.add(this.model);
    }
    super.initialize(options);

    this.model.fetch();

    this.listenTo(this.model, "change:status", () => {
      this.renderViews();
    });
  }

  createView(cls, options) {
    if (cls === StatusDetail) {
      let status = this.model.attributes.status;
      if (status) {
        options.model = new bv.BaseModel(status);
        if (options.model) {
          return new cls(options);
        }
      }
    } else if (cls == RewardList) {
      options.collection = new Backbone.Collection();
      options.collection.set(this.model.attributes.rewards);
      return new cls(options);
    }

    return null;
  }

  fetchModel(id) {
    return new Promise((resolve, reject) => {
      let model = this.collection.findWhere({
        id: parseInt(id)
      });
      if (! model) {
        Backbone.app.collections.tasks.fetch({
          success: () => {
            let model = this.collection.findWhere({
              id: parseInt(id)
            });
            if (! model) {
              reject(new Error("Sorry this task no longer exists"))
            } else {
              resolve(model);
            }
          }
        })
      } else {
        resolve(model);
      }
    });
  }

  handleComplete(e) {
    e.preventDefault();
    var promise = this.model.progress()
      .then((retval) => {
        this.model.set(retval);
      })
      .catch(function() {
        console.log("error");
      });
  }
}


module.exports = TaskDetail
