var Backbone = require("backbone");
var bv = require("backbone_views");
var Provider = require("../models/provider");
var _ = require("underscore");
var toastr = require("toastr");


class ProviderDetail extends bv.DetailView {
  get back() { return {title: "Dashboard", url: "#providers/"}; }
  get title() { return this.model.attributes.name; }
  get template() {
    return _.template(`
      <div class="containter">
        <h2 data-name></h2>
        <div data-description></div>
        <button id="provider-subscribe" class="btn">Subscribe</button>
      </div>
    `);
  }
  get events() {
    return {
      "click #provider-subscribe": "handleSubscribe",
    };
  }
  get collection() {
    return Backbone.app.collections.providers;
  }

  initialize(options) {
    this.model = new Provider();
    super.initialize(options);
    this.fetchModel(options.kwargs.id)
      .then((model) => this.model.set(model.attributes))
      .catch((err) => toastr.error(err));
  }

  fetchModel(id) {
    return new Promise((resolve, reject) => {
      let model = this.collection.findWhere({
        id: parseInt(id)
      });
      if (! model) {
        this.collection.fetch({
          success: () => {
            let model = this.collection.findWhere({
              id: parseInt(id)
            });
            if (! model) {
              reject(new Error("Sorry this provider no longer exists"))
            } else {
              resolve(model);
            }
          }
        })
      } else {
        resolve(model);
      }
    });
  }

  handleSubscribe() {
    this.model.subscribe()
      .then(() => toastr.success("You are subscribed"))
      .catch((e) => toastr.error(e));
  }
}


module.exports = ProviderDetail
