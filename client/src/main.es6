var $ = require("jquery");
window.$ = $;
window.jQuery = $;

require("jquery.transit");
require("bootstrap");

var bv = require("backbone_views");
var Application = require("leitjohn-webpack-starter/src/application");
var User = require("./models/user");
var _ = require("underscore");
var Backbone = require("backbone");


class Socket {

  constructor(endpoint) {
    _.extend(this, Backbone.Events);
    this.endpoint = endpoint || "ws://192.168.1.144:8011/ws";
    this.connect();
  }

  connect() {
    this.socket = new WebSocket(this.endpoint);
    this.socket.onmessage = (e) => {
      var data = e.data;
      try {
        data = JSON.parse(e.data);
        this.trigger(data[0], data[1]);
        console.log("socket message:", data[0]);
      } catch(err) {
        this.trigger("_msg", data);
        console.log("socket message parse error:", err, data);
      }
    };
    this.socket.onopen = () => {
      console.log("opened socket");
      this.trigger("_open");
    };
    this.socket.onclose = () => {
      console.log("closed socket");
      this.trigger("_close");
      console.log("will attempt reconnect in 5 seconds");
      _.delay(this.connect.bind(this), 5000);
    };
    this.socket.onerror = (e) => {
      console.log("socket error", e);
      this.trigger("_error", e);
      console.log("will attempt reconnect in 15 seconds");
      _.delay(this.connect.bind(this), 15000);
    };
  }

  send(msg, data=null) {
    this.socket.send(JSON.stringify([msg, data]))
  }
}

class Main extends Application {
  get routes() {
    return {
      "rewards/": require("./views/reward_list"),
      "provider/:id/": require("./views/provider_detail"),
      "providers/": require("./views/provider_list"),
      "task/:id/": require("./views/task_detail"),
      "tasks/": require("./views/task_list"),
      "login/": require("./views/login"),
      "": require("./views/task_list"),
    };
  }
  get routingSelector() { return "#main"; }

  initialize(options) {
    this.title = "Chore Market";
    this.collections = {
      providers: require("./models/provider").collection(),
      tasks: require("./models/task").collection(),
    }
    this.user = new User(this.session.get("user", {}));
    if (! this.user.get("token") && document.location.hash != "#login/") {
      if (document.location.hash) {
        this.session.set("next", document.location.hash);
      }
      document.location.hash = "login/";
    }
    this.listenTo(this.user, "change", () => {
      console.log("saving json");
      this.session.set("user", this.user.toJSON());
    });

    // all views will animate
    bv.MixinView.prototype.global_mixins = [
      require("./mixins/animate"),
      require("./mixins/titled")
    ];

    super.initialize(options);

    // update from the server
    this.websocket();
    this.listenTo(this.socket, "task", (data) => {
      let task = this.collections.tasks.get(data.id);
      // TODO: create if not found
      console.log(data);
      task.set(data);
    });

    // make the title do a backwards animations
    $(".navbar-brand").on("click", (e) => {
      e.preventDefault();
      this.go($(e.target).attr("href"));
    });

    // make the menu close after clicking an item
    $(".navbar-collapse a").click((e) => {
      $(".navbar-toggle").click();
    });

    // fix width
    this.listenTo(this, "render:post", (e) => {
    });


    // auto logout
    this.listenTo(Backbone, "ajax:401", () => {
      toastr.error("Sorry, your session has expired (401)");
      this.go("#login/");
    });
    this.listenTo(Backbone, "ajax:403", () => {
      toastr.error("Sorry, your login credentials are invalid (403)");
      this.go("#login/");
    });
  }
  resize(e) {
    super.resize(e);
    $("#main, #main-nav").css("width", $(window).width());
  }
  websocket() {
    this.socket = new Socket(this.options.websocketEndpoint);
    this.listenTo(this.socket, "_open", () => {
      console.log("connected");
      this.socket.send("authorize", this.user.get("token"));
      setTimeout(() => this.socket.send("foo"), 500);
    });
  }
  template() {
    return '<div id="content"></div>';
  }
  setTitle(text, link) {
    console.error("set title", text, link);
    if ( ! text) {
      $(".navbar-brand").text(this.title).attr("href", "#/");
    } else {
      $(".navbar-brand").html(
        "<i class='fa fa-arrow-circle-left'></i> " + text).attr("href", "#" + link);
    }
  }
}

window.Main = Main
module.exports = Main;
