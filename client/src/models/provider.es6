var bv = require("backbone_views");
var $ = require("jquery");


class Provider extends bv.BaseModel {
  static get slug() {
    return "providers";
  }
  static get urlRoot() {
    return "providers";
  }

  set(data) {
    if (data.id) {
      data.link = `#provider/${data.id}/`;
    }
    super.set(data);
  }

  subscribe() {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: this.url("subscribe"),
        type: "post",
        success: resolve,
        error: reject
      });
    });
  }

}
Provider.prototype.urlRoot = "providers";


module.exports = Provider;
