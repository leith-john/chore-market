var $ = require("jquery");
var bv = require("backbone_views");


class User extends bv.BaseModel {
  static get defaults() {
    return {
      username: "Anonymous",
      first_name: "",
      last_name: "",
      email: "",
      authenticated: false,
      token: ""
    };
  }

  login(username, password) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: bv.BaseModel.prototype.apiRoot + "/token-auth/",
        type: "POST",
        data: {
          username: username,
          password: password
        },
        success: (retval) => {
          if (retval.token) {
            this.set(retval);
          }
          resolve(retval);
        },
        error: function(e) {
          reject(e.responseJSON);
        }
      })
    });
  }
}


module.exports = User;
