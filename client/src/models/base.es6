var bv = require("backbone_views");
var $ = require("jquery");


class BaseModel extends bv.BaseModel {

  post(action, data) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: this.url(action),
        type: "POST",
        data: data,
        success: function(retval) {
          resolve(retval);
        },
        error: function(retval) {
          reject(new Error(retval.responseJSON));
        }
      })
    });
  }
}

module.exports = BaseModel
