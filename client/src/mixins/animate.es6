var Backbone = require("backbone");
var _ = require("underscore");
var $ = require("jquery");


class AnimateMixin {

  _fadeCurrent(e) {
    if (! e.in && ! e.reverse) {
      this.$el.css({
        zIndex: 1,
      }).transition({
        opacity: 0.75,
        duration: this.duration,
      });
      e.done = true;
    }
  }
  _restoreOld(e) {
    if (e.in && e.reverse) {
      this.$el.css({
        opacity: 0.75,
        zIndex: 1,
      }).transition({
        opacity: 1,
        duration: this.duration,
      });
      e.done = true;
    }
  }
  _slideIn(e) {
    if (e.in && ! e.reverse) {
      this.$el.css({
        x: this.width,
      }).transition({
        x: 0,
        duration: this.duration,
      });
      e.done = true;
    }
  }
  _slideOut(e) {
    this.$el.transition({
      x: this.width,
      duration: this.duration,
    });
  }

  initialize(options={}) {
    this.duration = 300;
    this._duration = undefined;
    this.easing = "linear";
    this._easing = undefined;

    if (! this.duration)
      this.duration = Backbone.router.transitionDuration;

    this.width = $(window).width();
    this.height = $(window).height();
    this.listenTo(Backbone, "routing:opened", (e) => {
      if (e.view == this) {
        e.in = true;
        this.animate(e);
        this.$el.css("min-height", "" + this.height + "px");
      }
    });
    this.listenTo(Backbone, "routing:closing", (e) => {
      if (e.view == this) {
        e.in = false;
        this.animate(e);
      }
    });
  }

  animate(e={}) {
    // fade out current
    this._fadeCurrent(e);

    // fade in old page (back)
    if (! e.done)
      this._restoreOld(e);

    // slide in
    if (! e.done)
      this._slideIn(e);

    // slide out (back)
    if (! e.done)
      this._slideOut(e);

    return null;
  }
}

module.exports = AnimateMixin;
