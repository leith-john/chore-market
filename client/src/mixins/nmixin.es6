var nunjucks = require("nunjucks/browser/nunjucks.js");
var moment = require("moment");

class nMixin {

  ntemplate(html) {
    return function(context) {
      let retval = nMixin.env.renderString(html, context);
      console.log(retval, context);
      return retval;
    }
  }

  initialize(options) {
    if (this.model) {
      this.listenTo(this.model, "change", () => {
        console.log('change', this.model.attributes);
        this.render.bind(this)()
      });
    }
  }
}

nMixin.env = new nunjucks.Environment();
nMixin.env.addFilter("date", function(obj, format="MMMM Do YYYY, H:mm a") {
  console.log("date", obj);
  return moment(obj).format(format);
});

module.exports = nMixin;
