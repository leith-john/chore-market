
class Titled {
  initialize(options) {
    this.listenTo(this, "render:post", () => {
      if (this.back) {
        Backbone.app.setTitle(this.back.title, this.back.url);
      }
    });
    this.listenTo(this, "view:context", (context) => {
      context.title = this.title
    });
  }
}

module.exports = Titled;
