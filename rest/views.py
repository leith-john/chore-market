from django.db.models import Q
from django.http import Http404
from rest_framework import viewsets
from rest_framework.decorators import (api_view, detail_route, list_route,
                                       permission_classes)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Provider, Task, UserReward
from .serializers import (ProviderSerializer, TaskSerializer,
                          UserRewardSerializer)


class ProviderViewSet(viewsets.ModelViewSet):
    serializer_class = ProviderSerializer

    def get_queryset(self):
        qs = Provider.objects.all()
        if self.request.user.is_authenticated():
            return qs.filter(
                Q(subscriptions__user=self.request.user) | Q(public=True)
            )
        else:
            return qs.filter(public=True)

    def has_permission(self, request, view):
        if view.action in ('create', 'update', 'delete'):
            return request.user.is_authenticated()
        else:
            return True

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated])
    def subscribe(self, request, pk):
        provider = self.get_queryset().get(pk=pk)
        request.user.subscriptions.create(provider=provider)
        return Response({"message": "OK"})

    @list_route()
    def my(self, request):
        if request.user.is_authenticated():
            qs = Provider.objects.filter(created_by=self.request.user)
            return Response(self.get_serializer(qs, many=True).data)
        return Response([])

    @list_route()
    def subscriptions(self, request):
        if request.user.is_authenticated():
            qs = Provider.objects.filter(subscriptions__user=self.request.user)
            return Response(self.get_serializer(qs, many=True).data)
        return Response([])

    @list_route(methods=['put'], permission_classes=[])
    def file(self, request):
        if "content" in request.data:
            import random
            from django.core.files.storage import default_storage
            from django.core.files.base import ContentFile
            r = str(random.randint(0, 9999))
            path = default_storage.save(
                'media/tmp/' + r,
                ContentFile(request.data['content'])
            )
            return Response("http://{}/{}".format(
                request.get_host(),
                path
            ))
        return Response([])


class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(
            provider__subscriptions__user=self.request.user).distinct()

    @detail_route(methods=["POST"])
    def complete(self, request, pk=None):
        try:
            task = self.get_queryset().get(pk=pk)
        except Task.DoesNotExist:
            raise Http404()
        task.progress(request.user)
        return Response(self.get_serializer(task).data)


class UserRewardViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRewardSerializer

    def get_queryset(self):
        return UserReward.objects.filter(user=self.request.user)
