
import kronos
from django.utils import timezone

from .models import Task


@kronos.register("@hourly")
def check_tasks():
    now = timezone.now()
    for task in Task.objects.exclude(repeats=None):
        if now > task.next:
            # task has expired, need to get the next
            # TODO: mark failed tasks
            task.save()
