import json

from django.contrib.auth.models import User
from django.test import TestCase

from .models import Provider, Task, Status, Subscription


class ClientRestTestCase(TestCase):

    def setUp(self):
        self.user1 = User.objects.create_user("user1", "", "user1")
        self.user2 = User.objects.create_user("user2", "", "user2")
        self.client.login(username="user1", password="user1")

        self.hands = Provider.objects.create(
            name="Hands", created_by=self.user1, public=True)
        self.feet = Provider.objects.create(
            name="Feet", created_by=self.user1)
        self.fingers = Provider.objects.create(
            name="Fingers", created_by=self.user2, public=True)
        self.toes = Provider.objects.create(
            name="Toes", created_by=self.user2)

        self.status_verified = Status.objects.create(
            provider=self.toes, name="Verified", complete=True)
        self.status_complete = Status.objects.create(
            provider=self.toes,
            name="Complete",
            next=self.status_verified)
        self.status_default = Status.objects.create(
            provider=self.toes,
            name="Open",
            default=True,
            next=self.status_complete)

        Task.objects.create(provider=self.hands, name="hands")
        Task.objects.create(provider=self.feet, name="feet")
        Task.objects.create(provider=self.fingers, name="fingers")
        self.task_toes = Task.objects.create(provider=self.toes, name="toes")

    def test_default_status_progression(self):
        # default status is assigned
        task = Task.objects.create(provider=self.toes, name="status?")
        self.assertEqual(task.status, self.status_default)

        task.progress()
        self.assertEqual(task.status, self.status_complete)

        task.progress()
        self.assertEqual(task.status, self.status_verified)

        # nothing happens
        task.progress()
        self.assertEqual(task.status, self.status_verified)

    def test_task_complete_flag(self):
        task = Task.objects.create(provider=self.toes, name="status?")
        self.assertEqual(task.complete, False)

        task.status = self.status_verified
        task.save()
        self.assertEqual(task.complete, True)

        task.status = self.status_complete
        task.save()
        self.assertEqual(task.complete, False)

    def test_provider_list(self):
        """
        should get a list of public providers
        private providers should be hidden
        """
        response = self.client.get("/api/providers/")
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(len(data), 3)
        self.assertEqual(
            [row['name'] for row in data],
            ["Hands", "Feet", "Fingers"])

    def test_my_subscriptions_list(self):
        """
        should show a list of subscribed providers
        """
        Subscription.objects.create(provider=self.fingers, user=self.user1)

        response = self.client.get("/api/providers/subscriptions/")
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(len(data), 3)
        self.assertEqual(
            [row['name'] for row in data],
            ["Hands", "Feet", "Fingers"])

    def test_my_providers_list(self):
        """
        should show a list of created_providers
        """
        response = self.client.get("/api/providers/my/")
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(len(data), 2)
        self.assertEqual(
            [row['name'] for row in data],
            ["Hands", "Feet"])

    def test_tasklist(self):
        """
        should show a list of user's tasks
        tasks in other providers should be hidden
        """
        Subscription.objects.get(provider=self.hands, user=self.user1).delete()
        Subscription.objects.create(provider=self.fingers, user=self.user1)

        response = self.client.get("/api/tasks/")
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(len(data), 2)
        self.assertEqual(
            [row['name'] for row in data],
            ["feet", "fingers"])

    def test_complete_task(self):
        """
        marks the task as complete
        """
        Subscription.objects.create(provider=self.toes, user=self.user1)
        response = self.client.post(
            "/api/tasks/{}/complete/".format(self.task_toes.pk))
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(data['status'], self.status_complete.pk)

    def test_complete_task_unassigned(self):
        """
        you can't mark a task complete that isn't provided to you
        """
        response = self.client.post(
            "/api/tasks/{}/complete/".format(self.task_toes.pk))
        self.assertEqual(response.status_code, 404)

    def test_verify_task(self):
        """
        TODO: marks the task as verified
        a reward is given to participants
        """
        # need to figure out status roles
