# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0004_auto_20151011_2053'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='status',
            options={'ordering': ('ordering',)},
        ),
    ]
