# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0006_auto_20151011_2100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currency',
            name='backend',
            field=models.CharField(choices=[('paypal', 'PayPal'), ('points', 'Points'), ('dollars', 'Dollars')], max_length=100),
        ),
        migrations.AlterField(
            model_name='status',
            name='complete',
            field=models.BooleanField(default=False, help_text='this status is considered complete'),
        ),
        migrations.AlterField(
            model_name='status',
            name='default',
            field=models.BooleanField(default=False, help_text='New Tasks will be created with this status'),
        ),
    ]
