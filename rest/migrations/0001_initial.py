# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('changed_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('backend', models.CharField(max_length=100, choices=[(b'paypal', b'PayPal'), (b'points', b'Points'), (b'dollars', b'Dollars')])),
                ('virtual', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('changed_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('public', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(related_name='my_providers', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('complete', models.BooleanField(default=False, help_text=b'this status is considered complete')),
                ('default', models.BooleanField(default=False, help_text=b'New Tasks will be created with this status')),
                ('next', models.ForeignKey(blank=True, to='rest.Status', null=True)),
                ('provider', models.ForeignKey(related_name='statuses', to='rest.Provider')),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('provider', models.ForeignKey(related_name='subscriptions', to='rest.Provider')),
                ('user', models.ForeignKey(related_name='subscriptions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(null=True, blank=True)),
                ('available', models.BooleanField(default=False, editable=False)),
                ('complete', models.BooleanField(default=False, editable=False)),
                ('provider', models.ForeignKey(to='rest.Provider')),
                ('status', models.ForeignKey(blank=True, to='rest.Status', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TaskReward',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(max_digits=15, decimal_places=3)),
                ('currency', models.ForeignKey(to='rest.Currency')),
                ('task', models.ForeignKey(to='rest.Task')),
            ],
        ),
        migrations.AddField(
            model_name='event',
            name='changed_by',
            field=models.ForeignKey(blank=True, to='rest.Profile', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='status',
            field=models.ForeignKey(blank=True, to='rest.Status', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='task',
            field=models.ForeignKey(to='rest.Task'),
        ),
        migrations.AddField(
            model_name='currency',
            name='provider',
            field=models.ForeignKey(to='rest.Provider'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='task',
            field=models.ForeignKey(to='rest.Task'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
