# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0005_auto_20151011_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='next',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='rest.Status', null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='rest.Status', null=True),
        ),
    ]
