from copy import copy

from rest_framework import serializers

from .models import Provider, Status, Task, TaskReward, UserReward, Currency


class ProviderSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = Provider
        fields = ["id", "name", "created_by"]

    def create(self, data):
        if 'request' not in self.context:
            raise Exception(
                "We require the `request` object in this serializer's context "
                "in order to set the `created_by` attribute"
            )
        user = self.context['request'].user
        if not user.is_authenticated():
            raise Exception(
                "Only authenticated users can create instances"
            )
        else:
            data['created_by'] = user
        return Provider.objects.create(**data)


class StatusSerializer(serializers.ModelSerializer):
    can_change = serializers.SerializerMethodField()
    next = serializers.SerializerMethodField()

    class Meta(object):
        model = Status

    def get_can_change(self, obj):
        request = self.context.get('request')
        return obj.default or \
            (request and request.user == obj.provider.created_by)

    def get_next(self, obj):
        if obj.next and "exclude_next" not in self.context:
            context = copy(self.context)
            context['exclude_next'] = True
            return StatusSerializer(obj.next, context=context).data
        return None


class CurrencySerializer(serializers.ModelSerializer):

    class Meta(object):
        model = Currency


class RewardSerializer(serializers.ModelSerializer):
    currency = serializers.SerializerMethodField()

    class Meta(object):
        model = TaskReward

    def get_currency(self, obj):
        return obj.currency.name


class TaskSerializer(serializers.ModelSerializer):
    provider = ProviderSerializer()
    status = StatusSerializer()
    rewards = RewardSerializer(many=True)

    class Meta(object):
        model = Task
        fields = [
            "id", "name", "description", "provider", "complete",
            "repeats", "interval", "next", "starting",
            'status', 'rewards'
        ]


class UserRewardSerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()

    class Meta(object):
        model = UserReward
