from django.apps import AppConfig


class RestConfig(AppConfig):
    name = 'rest'
    verbose_name = "rest"

    def ready(self):
        from . import listeners
