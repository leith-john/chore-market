
# from django.core.signals import request_finished
import json

from channels import Group
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from .models import Provider, Task
from .serializers import TaskSerializer


@receiver(pre_save, sender=Task)
def task_log(sender, instance=None, created=False, **kwargs):
    if instance.pk:
        old = Task.objects.get(pk=instance.pk)
        if instance.status != old.status:
            instance.events.create(
                task=instance,
                status=instance.status,
            )


@receiver(post_save, sender=Task)
def task_ws_connection(sender, instance=None, **kwargs):
    # key = "provider-{}".format(instance.provider.name)
    data = TaskSerializer(instance).data
    Group("chat").send(dict(content=json.dumps(["task", data])))


@receiver(post_save, sender=Provider)
def first_subscriber(sender, instance=None, created=False, **kwargs):
    if created:
        instance.subscriptions.create(user=instance.created_by)


@receiver(pre_save, sender=Task)
def task_default_status(sender, instance=None, **kwargs):
    if not instance.status:
        statuses = instance.provider.statuses.filter(default=True)
        if statuses.exists():
            status = statuses[0]
            instance.status = status
            instance.complete = status.complete


@receiver(pre_save, sender=Task)
def task_complete_flag(sender, instance=None, **kwargs):
    if instance.status:
        instance.complete = instance.status.complete
