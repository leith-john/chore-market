from django.contrib import admin

from .models import (Currency, Event, Provider, Status, Subscription, Task,
                     TaskReward, UserReward)


class StatusInline(admin.TabularInline):
    model = Status
    extra = 0

    def get_formset(self, request, instance, *args, **kwargs):
        self.parent_instance = instance
        return super().get_formset(request, instance, *args, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == "next":
            kwargs['queryset'] = Status.objects.filter(
                provider=self.parent_instance)
        return super().formfield_for_dbfield(db_field, **kwargs)


class ProviderAdmin(admin.ModelAdmin):
    inlines = [StatusInline]
    search_fields = ("name", )


class RewardInline(admin.TabularInline):
    model = TaskReward
    extra = 0

    def get_formset(self, request, instance, *args, **kwargs):
        self.parent_instance = instance
        return super().get_formset(request, instance, *args, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == "status":
            kwargs['queryset'] = Status.objects.filter(
                provider=self.parent_instance)
        return super().formfield_for_dbfield(db_field, **kwargs)


class TaskAdmin(admin.ModelAdmin):
    inlines = [RewardInline]
    list_display = ("name", "provider", "status")
    list_editable = ("status",)
    search_fields = ("name", "description")
    instance = None

    def get_form(self, request, instance, *args, **kwargs):
        self.instance = instance
        return super().get_form(request, instance, *args, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == "status":
            if self.instance:
                kwargs['queryset'] = Status.objects.filter(
                    provider=self.instance.provider)
            else:
                kwargs['queryset'] = Status.objects.none()
        return super().formfield_for_dbfield(db_field, **kwargs)


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ("user", "provider")


class EventAdmin(admin.ModelAdmin):
    search_fields = ("task__name", "status__name")
    list_filter = ("changed_by", "changed_on")
    list_display = ("task", "status", "changed_on", "changed_by")


admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(Provider, ProviderAdmin)
admin.site.register(Currency)
admin.site.register(UserReward)
admin.site.register(Event, EventAdmin)
